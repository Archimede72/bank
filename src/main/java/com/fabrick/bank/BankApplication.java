package com.fabrick.bank;

import com.fabrick.bank.frio.FrIOConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@AllArgsConstructor
@EnableAspectJAutoProxy
@EnableWebMvc
public class BankApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	FrIOConfig frIOConfig() {
		return new FrIOConfig();
	}

}
