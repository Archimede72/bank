package com.fabrick.bank.frio;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;

public class FrIOConfig {

    @Value("${app.external.url.balance}")
    private String balanceUrl;
    @Value("${app.external.url.transaction}")
    private String transactionUrl;
    @Value("${app.external.url.moneyTransfers}")
    private String moneyTransfersUrl;
    @Value("#{${draft-paramiters}}")
    private MultiValueMap<String, String> frIOHeaders;


    public String generateBalanceUrl(Long accountId) {
        return String.format(balanceUrl, accountId);
    }

    public String generateTransactionUrl(Long accountId, LocalDate fromAccountingDate, LocalDate toAccountingDate) {
        return String.format(transactionUrl, accountId, fromAccountingDate, toAccountingDate);
    }

    public String generateMoneyTransferUrl(Long accountId) {
        return String.format(moneyTransfersUrl, accountId);
    }

    public MultiValueMap<String, String> frIOHeaders () {
        return new HttpHeaders(frIOHeaders);
    }

}
