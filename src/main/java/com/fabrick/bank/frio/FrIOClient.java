package com.fabrick.bank.frio;

import com.fabrick.bank.shared.aspect.BankLogger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FrIOClient {

    private final FrIOConfig frIOConfig;
    private final RestTemplate restTemplate;

    @BankLogger(message = "FrIOPlt GET request")
    public <T> T frIOGet(String url, Class<T> clazz) throws RestClientException {
        HttpEntity<T> entity = new HttpEntity(frIOConfig.frIOHeaders());
        return restTemplate.exchange(url, HttpMethod.GET, entity, clazz).getBody();
    }

    @BankLogger(message = "FrIOPlt POST request")
    public <T> T frIOPost(String url, Class<T> clazz, String jsonBody) throws RestClientException {
        HttpEntity<T> entity = new HttpEntity(jsonBody, frIOConfig.frIOHeaders());
        return restTemplate.postForEntity(url, entity, clazz).getBody();
    }

}
