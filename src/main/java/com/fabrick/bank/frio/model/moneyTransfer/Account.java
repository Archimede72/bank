package com.fabrick.bank.frio.model.moneyTransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account{

    private String accountCode = "IT23A0336844430152923804660";
}