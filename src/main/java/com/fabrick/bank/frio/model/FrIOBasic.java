package com.fabrick.bank.frio.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class FrIOBasic {

    private String status;
    private List<FrIOError> error;

}
