package com.fabrick.bank.frio.model.moneyTransfer;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaxRelief{
    private String taxReliefId = "L449";
    @JsonProperty("isCondoUpgrade")
    private boolean isCondoUpgrade = false;
    private String creditorFiscalCode = "56258745832";
    private String beneficiaryType = "NATURAL_PERSON";
    private NaturalPersonBeneficiary naturalPersonBeneficiary = new NaturalPersonBeneficiary();
}
