package com.fabrick.bank.frio.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FrIOTransactionPayload {
    private List<FrIOTransactionItem> list;
}
