package com.fabrick.bank.frio.model.transaction;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Type{
    public String enumeration;
    public String value;
}
