package com.fabrick.bank.frio.model.moneyTransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NaturalPersonBeneficiary{
    private String fiscalCode1 = "MRLFNC81L04A859L";
}