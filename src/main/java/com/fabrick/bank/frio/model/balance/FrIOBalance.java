package com.fabrick.bank.frio.model.balance;

import com.fabrick.bank.frio.model.FrIOBasic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class FrIOBalance extends FrIOBasic {
    private FrIOBalancePayload payload;
}
