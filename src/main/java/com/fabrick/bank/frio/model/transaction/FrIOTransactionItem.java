package com.fabrick.bank.frio.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FrIOTransactionItem {
    public String transactionId;
    public String operationId;
    public String accountingDate;
    public String valueDate;
    public Type type;
    public double amount;
    public String currency;
    public String description;
}
