package com.fabrick.bank.frio.model.transaction;

import com.fabrick.bank.frio.model.FrIOBasic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class FrIOTransaction extends FrIOBasic {
    private FrIOTransactionPayload payload;
}
