package com.fabrick.bank.frio.model.balance;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FrIOBalancePayload {

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    private double balance;
    private double availableBalance;
    private String currency;
}
