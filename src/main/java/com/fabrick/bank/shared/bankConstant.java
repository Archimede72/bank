package com.fabrick.bank.shared;

import java.time.LocalDate;

public interface bankConstant {
    Long ACCOUNT_ID = 14537780L;
    String MSG_SALDO = "Il suo saldo è: %s EURO";

    LocalDate START_DATE = LocalDate.parse("2017-03-08");
    String ERROR_DATE_NON_VALIDE = "Range temporale invalido";
    String KO = "KO";
    String ERROR_TECNICO = """
        {
            "code": "API000",
            "description": "Errore tecnico  La condizione BP049 non e' prevista per il conto id %s
        }
        """;

}
