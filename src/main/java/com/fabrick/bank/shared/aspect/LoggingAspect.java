package com.fabrick.bank.shared.aspect;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;

@Log4j2
@Aspect // Annoto la classe come Aspetto
@Component
public class LoggingAspect {

    @Around("@annotation(BankLogger)")
    public Object bankLoggerTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();

        final String methodName = joinPoint.getSignature().getName();
        final MethodSignature sig = (MethodSignature) joinPoint
                .getSignature();
        Method method = sig.getMethod();
        BankLogger annotation = null;
        method = joinPoint.getTarget().getClass()
                .getDeclaredMethod(methodName, method.getParameterTypes());
        annotation = method.getAnnotation(BankLogger.class);

        Object[] args = joinPoint.getArgs();
        String message = String.format("the method %s is in execution with this message: %s \n\t\t args = %s", methodName,annotation.message(),Arrays.toString(args));
        log.info( message);

        Object proceed = null;
        try {
            proceed = joinPoint.proceed();
        } finally {
            //Do Something useful, If you have
        }

        long executionTime = System.currentTimeMillis() - start;
        log.info(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }

    @AfterThrowing(value = "(execution(com.fabrick.bank* *.*(..)))", throwing = "e")
    public void logException(JoinPoint thisJoinPoint, RuntimeException e) {
        log.error(thisJoinPoint + " -> " + e);
    }

//    @AfterThrowing(pointcut = "execution(public * com.fabrick.bank*..*.*(..))", throwing = "e")
//    public void myAdvice(RuntimeException e){
//        Thread.setDefaultUncaughtExceptionHandler((t, e1) ->
//                System.out.println("Caught " + e1.getMessage()));
//       // System.out.println("Worker returned " + worker.print());
//    }

//    @AfterThrowing(pointcut = "execution(* *(..))", throwing = "ex")
//    public void logAfterThrowingAllMethods(Exception ex) throws Throwable {
//        String message = ex.getMessage() ;
//        log.error(message);
//    }


}