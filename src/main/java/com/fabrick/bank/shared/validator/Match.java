package com.fabrick.bank.shared.validator;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Match {
    String field();
    String message() default "";
}

