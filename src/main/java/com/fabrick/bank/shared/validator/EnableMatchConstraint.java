package com.fabrick.bank.shared.validator;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MatchValidator.class)
@Documented
public @interface EnableMatchConstraint {

    String message() default "Fields must match!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

class MatchValidator implements ConstraintValidator<EnableMatchConstraint, Object> {

    @Override
    public void initialize(final EnableMatchConstraint constraint) {}

    @Override
    public boolean isValid(final Object o, final ConstraintValidatorContext context) {
        boolean result = true;
        try {
            String mainField_name, secondField_name, message;
            Object firstObj, secondObj;

            final Class<?> clazz = o.getClass();
            final Field[] fields = clazz.getDeclaredFields();

            for (Field mainField : fields) {
                if (mainField.isAnnotationPresent(Match.class)) {
                    mainField_name = mainField.getName();
                    secondField_name = mainField.getAnnotation(Match.class).field();
                    Field secondField = clazz.getDeclaredField(secondField_name);

                    mainField.setAccessible(true);
                    secondField.setAccessible(true);

                    firstObj = mainField.get(o);
                    secondObj = secondField.get(o);

                    result = firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);

                    if (!result) {
                        context.disableDefaultConstraintViolation();

                        message = mainField.getAnnotation(Match.class).message();
                        if (message == null || "".equals(message))
                            message = "Fields " + mainField_name + " and " + secondField_name + " must match!";

                        context.buildConstraintViolationWithTemplate(message).addPropertyNode(mainField_name).addConstraintViolation();
                        break;
                    }
                }
            }
        } catch (final Exception e) {
            // ignore
            // e.printStackTrace();
        }
        return result;
    }
}