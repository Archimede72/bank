package com.fabrick.bank.shared.error;

import com.fabrick.bank.bankAccount.Violation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationErrorResponse extends RuntimeException {
    private List<Violation> violations = new ArrayList<>();
}