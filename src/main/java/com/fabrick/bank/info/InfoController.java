package com.fabrick.bank.info;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/info")
public class InfoController {

    @Value("#{${info-app}}")
    private Map<String, String> appInfo;

    @GetMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String getInfo() {
        String result = "\n[Application info]";
         return appInfo.entrySet().stream()
                 .map(x -> "\n\t" + x.getKey() + "\t\t: " + x.getValue())
                 .reduce(result, (acc, y) -> acc + y);
    }

    @GetMapping("/version")
    @ResponseBody
    public String getVersion() {
        return appInfo.get("build_version");
    }
}