package com.fabrick.bank.bankAccount;

import com.fabrick.bank.frio.model.moneyTransfer.Account;
import com.fabrick.bank.frio.model.moneyTransfer.Creditor;
import com.fabrick.bank.frio.model.moneyTransfer.FrIOMoneyTransfer;
import com.fabrick.bank.frio.model.moneyTransfer.TaxRelief;
import org.springframework.stereotype.Service;

@Service
public class MoneyTransferConvert {

    public FrIOMoneyTransfer convert_MoneyTransferTOFrIOMoneyTransfer(MoneyTransfer moneyTransfer) {
        return FrIOMoneyTransfer.builder()
                .executionDate(moneyTransfer.getExecutionDate())
                .description(moneyTransfer.getDescription())
                .amount(moneyTransfer.getAmount())
                .currency(moneyTransfer.getCurrency())
                .creditor(Creditor.builder()
                        .name(moneyTransfer.getReceiverName())
                        .account(new Account())
                        .build())
                .taxRelief(new TaxRelief())
                .build();
    }
}
