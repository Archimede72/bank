package com.fabrick.bank.bankAccount;

import static com.fabrick.bank.shared.bankConstant.ERROR_DATE_NON_VALIDE;
import static com.fabrick.bank.shared.bankConstant.START_DATE;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;

@Service
public class BankAccountValidator {

    public void validatorDate(LocalDate startOfRange, LocalDate endOfRange)  {
        if ( startOfRange.isAfter(endOfRange) ||
             endOfRange.isAfter(LocalDate.now()) ||
             startOfRange.isBefore(START_DATE)
        ) throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, ERROR_DATE_NON_VALIDE);
    }

}
