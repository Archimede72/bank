package com.fabrick.bank.bankAccount;

import static com.fabrick.bank.shared.bankConstant.ERROR_TECNICO;
import static com.fabrick.bank.frio.model.moneyTransfer.FrIOMoneyTransfer.objectToJson;

import com.fabrick.bank.shared.aspect.BankLogger;
import com.fabrick.bank.frio.FrIOClient;
import com.fabrick.bank.frio.FrIOConfig;
import com.fabrick.bank.frio.model.FrIOBasic;
import com.fabrick.bank.frio.model.balance.FrIOBalance;
import com.fabrick.bank.frio.model.balance.FrIOBalancePayload;
import com.fabrick.bank.frio.model.moneyTransfer.FrIOMoneyTransfer;
import com.fabrick.bank.frio.model.transaction.FrIOTransaction;
import com.fabrick.bank.frio.model.transaction.FrIOTransactionItem;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/banking-account/accounts")
@AllArgsConstructor
@Validated
public class BankAccountController {

    private final FrIOConfig frIOConfig;
    private final FrIOClient frIOClient;
    private final MoneyTransferConvert moneyTransferConvert;
    private final BankAccountValidator bankAccountValidator;

    @BankLogger(message = "Balance request")
    @GetMapping("/{accountId}/balance")
    public FrIOBalancePayload accountBalance(@PathVariable Long accountId) {
        String url = frIOConfig.generateBalanceUrl(accountId);
        FrIOBalance frIOBalance = frIOClient.<FrIOBalance>frIOGet(url, FrIOBalance.class);
        return frIOBalance.getPayload();
    }

    @BankLogger(message = "Transactions list request within range")
    @GetMapping("/{accountId}/transactions")
    public List<FrIOTransactionItem> transactions(
            @PathVariable Long accountId,
            @RequestParam("fromAccountingDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromAccountingDate,
            @RequestParam("toAccountingDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toAccountingDate
    ) {
        bankAccountValidator.validatorDate(fromAccountingDate, toAccountingDate);

        String url = frIOConfig.generateTransactionUrl(accountId, fromAccountingDate, toAccountingDate);
        FrIOTransaction frIOTransaction = frIOClient.<FrIOTransaction>frIOGet(url, FrIOTransaction.class);
        return frIOTransaction.getPayload().getList();
    }

    @BankLogger(message = "Money Transfer request")
    @PostMapping("/{accountId}/transactions")
    public FrIOBasic moneyTransfer(
            @Min(5) @PathVariable Long accountId,
            @Valid @RequestBody MoneyTransfer moneyTransfer) {

        String url = frIOConfig.generateMoneyTransferUrl(accountId);
        FrIOMoneyTransfer frIOMoneyTransfer = moneyTransferConvert.convert_MoneyTransferTOFrIOMoneyTransfer(moneyTransfer);
        try {
            frIOClient.<FrIOBasic>frIOPost(url, FrIOBasic.class, objectToJson(frIOMoneyTransfer));
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, String.format(ERROR_TECNICO, accountId) );
        }
        return null;
    }

}
