package com.fabrick.bank.bankAccount;

import com.fabrick.bank.shared.error.BadArgumentsException;
import com.fabrick.bank.shared.error.ValidationErrorResponse;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.web.client.RestClientException;

// @ControllerAdvice
// @ControllerAdvice("com.fabrick.bank.junit.bankAccount")
@ControllerAdvice(assignableTypes = BankAccountController.class)
@AllArgsConstructor
@Log4j2
public class BankAccountControllerAdvice {

    private ResponseEntity<String> error(final Exception exception, final HttpStatus httpStatus, final String logRef) {
        final String message = (logRef != null) ? logRef : Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
        log.error(exception);
        return new ResponseEntity<>(message, httpStatus);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handlerIllegalArgumentException(final IllegalArgumentException e) {
        return error(e, HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
    }

    @ExceptionHandler({HttpStatusCodeException.class, HttpClientErrorException.class, RestClientException.class, BadArgumentsException.class})
    public ResponseEntity<String> handlerHttpStatusCodeException(final HttpStatusCodeException e) {
        String statusText = e.getStatusText();
        return error(e, e.getStatusCode(), null != statusText ? statusText : e.getMessage());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    List<Violation> onConstraintValidationException(ConstraintViolationException e) {
        List<Violation> violations = e.getConstraintViolations().stream()
                .map(Violation::new).collect(Collectors.toList());
        return new ValidationErrorResponse(violations).getViolations();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    List<Violation> onMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<Violation> violations = e.getBindingResult().getFieldErrors().stream()
                .map(Violation::new).collect(Collectors.toList());
        return new ValidationErrorResponse(violations).getViolations();
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<String> showCustomMessage(Exception e){
        return error(e, HttpStatus.BAD_REQUEST, e.getMessage());
    }

}
