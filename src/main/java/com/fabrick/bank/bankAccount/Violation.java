package com.fabrick.bank.bankAccount;

import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Violation {
    private final String fieldName;
    private final String message;

    public Violation(ConstraintViolation violation) {
        this.fieldName = violation.getPropertyPath().toString();
        this.message = violation.getMessage();
    }

    public Violation(FieldError fieldError) {
        this.fieldName = fieldError.getField();
        this.message = fieldError.getDefaultMessage();
    }
}