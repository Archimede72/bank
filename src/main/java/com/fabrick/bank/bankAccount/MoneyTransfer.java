package com.fabrick.bank.bankAccount;


import com.fabrick.bank.shared.validator.EnableMatchConstraint;
import com.fabrick.bank.shared.validator.IpAddress;
import com.fabrick.bank.shared.validator.Match;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransfer {

    @NotBlank(message = "specificare un receiverName valido")
    private String receiverName;
    @NotBlank(message = "specificare una descrizione valida")
    private String description;
    @NotBlank(message = "specificare una valuta valida")
    private String currency;
    @Min(value = 5, message = "specificare un valore valido")
    private double amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate executionDate;
}