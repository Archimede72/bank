drop table saldo;
create table saldo (
                       account_id int8 not null,
                       value float8 not null,
                       primary key (account_id)
);
insert into saldo (account_id, value) VALUES (14537780, 999999.34);
