package com.fabrick.bank.bdd;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.web.client.TestRestTemplate;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

@AutoConfigureMockMvc
public class VersionDefs extends CucumberSpringContextConfiguration{

    private static final String BASE_URL = "http://localhost:";
    private static final String PATH_VERSION = "/info/version";

    @Autowired
    private TestRestTemplate restTemplate;

    private String version;

    @Given("as user i wont know the app version")
    public void asUser() {
    }

    @When("the client calls version")
    public void theClientCallsVersion() {
        version = restTemplate.getForObject(BASE_URL + port + PATH_VERSION, String.class);
    }

    @Then("the client receives server version (.*)$")
    public void theClientReceivesServerVersion(String ver) {
        assertEquals(ver, version);
    }

}
