package com.fabrick.bank.bankAccount;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@SpringBootTest
class ReactorTests {

	@Test
	void thread() {
		System.out.println("\n\n\t\t***** specificThread *****");
		Flux<Integer> flux = Flux.range(0, 2)
				.log()
				.doOnNext(System.out::println)
				.map(i -> {
					System.out.println("Mapping for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				});

		flux.subscribe(s -> {
			System.out.println("Received " + s + " via " + Thread.currentThread().getName());
		});

		flux.subscribe(s -> {
			System.out.println("Received " + s + " via " + Thread.currentThread().getName());
		});
	}

	@Test
	void specificThread() {
		System.out.println("\n\n\t\t***** specificThread *****");
		Flux<Integer> flux = Flux.range(0, 2)
//				.log()
//				.doOnNext(System.out::println)
				.map(i -> {
					System.out.println("Mapping for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				});

		//create a runnable with flux subscription
		Runnable r = () -> flux.subscribe(s -> {
			System.out.println("Received " + s + " via " + Thread.currentThread().getName());
		});

		Thread t1 = new Thread(r, "t1");
		Thread t2 = new Thread(r, "t2");

		//lets start the threads. (this is when we are subscribing to the flux)
		System.out.println("Program thread :: " + Thread.currentThread().getName());
		t1.start();
		t2.start();
	}

	@Test
	void immediate() {
		System.out.println("\n\n\t\t***** immediate *****");
		Flux<Integer> flux = Flux.range(0, 2)
				.publishOn(Schedulers.immediate())
				.map(i -> {
					System.out.println("Mapping for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				});

		//create a runnable with flux subscription
		Runnable r = () -> flux.subscribe(s -> {
			System.out.println("Received " + s + " via " + Thread.currentThread().getName());
		});

		Thread t3 = new Thread(r, "t3");
		Thread t4 = new Thread(r, "t4");

		//lets start the threads. (this is when we are subscribing to the flux)
		System.out.println("Program thread :: " + Thread.currentThread().getName());
		t3.start();
		t4.start();
	}

	@Test
	void single() {
		System.out.println("\n\n\t\t***** single *****");

		Flux<Integer> flux = Flux.range(1, 5)
//				.log()
//				.doOnNext(System.out::println)

//				.publishOn(Schedulers.single())
//				.publishOn(Schedulers.newSingle("valenz"))

				.map(i -> {
					System.out.println("Mapping for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				})
				;

		Stream.of(1,2,3,4,5,6,7,8,9)
				.peek(System.out::println)
				.forEach(
					x -> flux.subscribe(s -> {
						System.out.println("Received " + s + " via " + Thread.currentThread().getName());
				})
		);
	}

	@Test
	void elastic_parallel() {
		System.out.println("\n\n\t\t***** elastic_parallel *****");

		Flux<Integer> flux = Flux.range(0, 3)
				.map(i -> {
					System.out.println("Mapping one for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				})
				.publishOn(Schedulers.boundedElastic())
				.map(i -> {
					System.out.println("Mapping two for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				})
				.publishOn(Schedulers.parallel())
				.map(i -> {
					System.out.println("Mapping three for " + i + " is done by thread " + Thread.currentThread().getName());
					return i;
				})
				;

		flux.subscribeOn(Schedulers.single())
			.subscribe(s -> {
				System.out.println("Received " + s + " via " + Thread.currentThread().getName());
			});


	}

}
