package com.fabrick.bank.bankAccount;

import static com.fabrick.bank.frio.model.moneyTransfer.FrIOMoneyTransfer.objectToJson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fabrick.bank.frio.FrIOClient;
import com.fabrick.bank.frio.model.FrIOBasic;
import com.fabrick.bank.frio.model.balance.FrIOBalance;
import com.fabrick.bank.frio.model.moneyTransfer.FrIOMoneyTransfer;
import com.fabrick.bank.frio.model.transaction.FrIOTransaction;
import com.fabrick.bank.shared.error.BadArgumentsException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;


@SpringBootTest
@AutoConfigureMockMvc
//@ContextConfiguration(
//        classes = {BankAccountController.class, FrIOClient.class, BankApplication.class, ExceptionHandler.class}
//)
class BankAccountControllerTest {

    @Value("${balance}")
    private String mock_balance;
    @Value("${transactions}")
    private String mock_transactions;
    @Value("${money-transfer}")
    private String mock_moneyTransfer;
    @Value("${money-transfer-invalid}")
    private String mock_moneyTransfer_invalid;

    @Autowired
    MockMvc mockMvc;
    @Autowired
    BankAccountController bankAccountController;
    @Autowired
    BankAccountControllerAdvice bankAccountControllerAdvice;
    @Autowired
    MoneyTransferConvert moneyTransferConvert;
    @Autowired
    Validator validator;

    @MockBean
    FrIOClient frIOClient;


    private ObjectMapper mapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();

    private <T> T  mok_object(Class<T> tClass, String json) throws JsonProcessingException {
        return mapper.readValue(json, tClass);
    }

    @BeforeClass
    void setUp() {
    }
    @AfterEach
    void tearDown() {
    }

    @Test
    void accountBalance_ok() throws Exception {
        // given
        String urlSandbox = "https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/14537780/balance";
        String url = "http://localhost:10001/banking-account/accounts/14537780/balance";

        // when
        FrIOBalance frIOBalance = this.<FrIOBalance>mok_object(FrIOBalance.class, mock_balance);
        when(frIOClient.<FrIOBalance>frIOGet(urlSandbox, FrIOBalance.class)).thenReturn(frIOBalance);

        mockMvc.perform(get(url))

        // then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.currency").exists())
                .andExpect(jsonPath("$.balance").isNotEmpty())
                .andExpect(jsonPath("$.currency").value("EUR"));
    }

    @Test
    void accountBalance_ko() throws Exception {
        // given
        String urlsandbox = "https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/14537781/balance";
        String url = "http://localhost:10001/banking-account/accounts/14537781/balance";

        // when
        when(frIOClient.<FrIOBalance>frIOGet(urlsandbox, FrIOBalance.class)).thenThrow(BadArgumentsException.class);
        mockMvc.perform(get(url))

        // then
                .andDo(print())
                .andDo(result ->  result.getResolvedException().getMessage())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof BadArgumentsException))
                .andExpect(status().isBadRequest());
    }

    @Test
    void transactions_ok() throws Exception {
        // given
        String urlSandbox = "https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/14537780/transactions?fromAccountingDate=2019-01-01&toAccountingDate=2019-12-01";
        String url = "http://127.0.0.1:10001/banking-account/accounts/14537780/transactions";
        FrIOTransaction frIOTransaction = this.<FrIOTransaction>mok_object(FrIOTransaction.class, mock_transactions);

        // when
        when(frIOClient.<FrIOTransaction>frIOGet(urlSandbox, FrIOTransaction.class)).thenReturn(frIOTransaction);
        mockMvc.perform(get(url)
                .param("fromAccountingDate", "2019-01-01")
                .param("toAccountingDate", "2019-12-01")
                )

        // then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").isNotEmpty())
                .andExpect(jsonPath("$[0].description").value("PD VISA CORPORATE 10"));
    }

    @Test
    void transactions_ko() throws Exception {
        // given
        String url = "http://127.0.0.1:10001/banking-account/accounts/14537780/transactions";

        // when
        mockMvc.perform(get(url)
                .param("fromAccountingDate", "2019-12-01")
                .param("toAccountingDate", "2019-01-01")
        )

        // then
                .andDo(print())
                .andDo(result ->  result.getResolvedException().getMessage())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof HttpClientErrorException))
                .andExpect(status().isBadRequest());
    }

    @Test
    void moneyTransfer_ko() throws Exception {
        // given
        String url = "http://localhost:10001/banking-account/accounts/14537780/transactions";
        String urlsandbox = "https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/14537780/payments/money-transfers";
        MoneyTransfer moneyTransfer = mok_object(MoneyTransfer.class, mock_moneyTransfer);
        FrIOMoneyTransfer frIOMoneyTransfer = moneyTransferConvert.convert_MoneyTransferTOFrIOMoneyTransfer(moneyTransfer);

        String frIOMoneyTransfer_json = objectToJson(frIOMoneyTransfer);

        // when
        when(frIOClient.<FrIOBasic>frIOPost(urlsandbox, FrIOBasic.class, frIOMoneyTransfer_json))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Invalid Amount: Monthly Limit Exceeding"));
        mockMvc.perform(
                post(url)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mock_moneyTransfer)
                )
        // then
                .andDo(print())
                .andDo(result -> result.getResolvedException().getMessage())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof HttpClientErrorException))
                .andExpect(status().isInternalServerError())
        ;
    }

    @Test
    void moneyTransfer_ko_invalidator() throws Exception {
        // given
        // Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        MoneyTransfer moneyTransfer = this.<MoneyTransfer>mok_object(MoneyTransfer.class, mock_moneyTransfer_invalid);

        // when
        Set<ConstraintViolation<MoneyTransfer>> violations = validator.validate(moneyTransfer);

        //then:
        assertFalse(violations.isEmpty());

        assertEquals(violations.size(), 1);

        ConstraintViolation<MoneyTransfer> violation = violations.iterator().next();

        assertEquals(violation.getMessage(),"specificare un receiverName valido");
        assertEquals("receiverName", violation.getPropertyPath().toString());
        assertNull(violation.getInvalidValue());
    }

    @Test
    void moneyTransfer_ko_invalidator_2() throws Exception {
        // given
        String url = "http://localhost:10001/banking-account/accounts/14537780/transactions";
        // when
        mockMvc.perform(post(url)
            .contentType(MediaType.APPLICATION_JSON)
            .content(mock_moneyTransfer_invalid))
        // then
        .andExpect(status().isBadRequest());
    }
}